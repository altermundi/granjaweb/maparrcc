<script>
    let center = [-40.196, -61.853]
    let zoom = 5
    {/* let tile = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    let tileOptions = {maxZoom: 19, attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'} */}
    let tile = "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png"
    let tileOptions = {attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>'}
        let data = [
            { name: "Funcionado", url: "assets/data/Redes_CARC_presente.csv", color: "green", icon: "home" },
            { name: "En desarrollo", url: "assets/data/Redes_CARC_futuras.csv", color: "blue", icon: "home" }
        ]

    const layers = {}
    const overlays = {}
    const markers = L.markerClusterGroup()

    const createPopup = (feature, ignoreKeys = ['icon', 'color']) => {
        let text = ''
        let properties = feature.properties

        Object.keys(properties).forEach(key => {
        if (properties[key] && !ignoreKeys.includes(key)) {
            text += `<strong>${key}:</strong> ${properties[key]}<br>`
        }
        })
        return text
    }

    const createLayer = (color='red', icon='home') => {
        return L.geoJson(null, {
            pointToLayer: (feature, latlng) => {
                const Marker = L.AwesomeMarkers.icon({
                    icon: feature.properties.icon || icon,
                    prefix: 'fa',
                    markerColor: feature.properties.color || color
                })      

                return L.marker(latlng, { icon: Marker })
            },

            onEachFeature: (feature, layer) => {
                layer.bindPopup(createPopup(feature, ['icon', 'color']))
            }
        })
    }

    // Set up initial map center and zoom level
    const map = L.map('map', {
        center: center, // EDIT latitude, longitude to re-center map
        zoom: zoom,  // EDIT from 1 to 18 -- decrease to zoom out, increase to zoom in
        scrollWheelZoom: true,
    })
        
    L.tileLayer(tile, tileOptions).addTo(map);

    data.forEach(entry => {
        layers[entry.name] = createLayer(entry.color, entry.icon);
        omnivore.csv(entry.url, null, layers[entry.name]).addTo(map);
        overlays[entry.name] = layers[entry.name];            
    })
        
    L.control.layers({}, overlays).addTo(map)

</script>